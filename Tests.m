//
//  Tests.m
//  DaftCountries_ObjCTests
//
//  Created by tomaszpaluch on 19/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DaftCountries_ObjC/RestService.h"

@interface DaftCountriesTests_RestServiceBasicFunctionality : XCTestCase

@end

@implementation DaftCountriesTests_RestServiceBasicFunctionality
RestService * restService = nil;

- (void)setUp {
    restService = [[RestService alloc] initWithReloadAction:^(void){}];

    NSURL *url = [[NSBundle bundleForClass:[self class]]
                    URLForResource: @"all" withExtension:@"json"];
    
    [restService TEST_getAllCountries:url];
    
    [NSThread sleepForTimeInterval:1.0f];
}

- (void)testGetLastInquiryCount_InquiryOK_Returns250 {
    XCTAssertEqual([restService getLastInquiryCount], 250);
}

- (void)testGetCountryNameAtSpecifiedIndex_Index178_ReturnsPoland {
    XCTAssertTrue([[restService getNameAt:178] isEqualToString:@"Poland"]);
}

@end

@interface DaftCountriesTests_RestServiceGetCountry : XCTestCase

@end

@implementation DaftCountriesTests_RestServiceGetCountry
RestService * restService2 = nil;
NSURL * url = nil;

- (void)setUp {
    restService2 = [[RestService alloc] initWithReloadAction:^(void){}];
    
    url = [[NSBundle bundleForClass:[self class]]
                  URLForResource: @"Poland" withExtension:@"json"];
}

- (void)testJSONSerialization_Poland_StructsEqual {
    struct RestCountryFullInfo fakeStruct;
    
    fakeStruct.name = @"Poland";
    fakeStruct.capital = @"Warsaw";
    fakeStruct.altSpellings = [NSArray arrayWithObjects: @"PL",@"Republic of Poland",@"Rzeczpospolita Polska", nil];
    fakeStruct.population = 38437239;
    fakeStruct.latlng = [NSArray arrayWithObjects:[NSNumber numberWithFloat:52.0],[NSNumber numberWithFloat:20.0], nil];
    fakeStruct.area = 312679.0;
    fakeStruct.flag = [NSURL URLWithString:@"https://restcountries.eu/data/pol.svg"];
    
    struct RestCountryFullInfo structFromJSON = [restService TEST_getCountryBy: url];
    
    XCTAssertTrue([self compareStruct:structFromJSON with:fakeStruct]);
}

- (BOOL) compareStruct:(struct RestCountryFullInfo)first with:(struct RestCountryFullInfo)second {
    if ([first.name isEqualToString:second.name]
        && [first.capital isEqualToString:second.capital]
        && first.population == second.population
        && first.area == second.area
        && [[first.flag absoluteString] isEqualToString:[second.flag absoluteString]])
        return true;
    else
        return false;
}
@end



