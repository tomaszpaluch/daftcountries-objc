//
//  CountryDetailsViewController.m
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import "CountryDetailsViewController.h"
#import <MapKit/MapKit.h>
#import <SVGKit/SVGKit.h>

@interface CountryDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *mainNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondaryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *capitalNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *populationCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaSizeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *flagImage;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation CountryDetailsViewController
dispatch_semaphore_t semaphore;

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    semaphore = dispatch_semaphore_create(0);
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareView];
}

- (void)prepareView {
    _mainNameLabel.text = @"...";
    _secondaryNameLabel.text = @"...";
    _capitalNameLabel.text = @"...";
    _populationCountLabel.text = @"...";
    _areaSizeLabel.text = @"...";
    
    _flagImage.backgroundColor = UIColor.grayColor;
    _flagImage.layer.masksToBounds = true;
    _flagImage.layer.borderWidth = 0.5;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_semaphore_signal(semaphore);
}

- (void)setupView:(struct RestCountryFullInfo) countryInformations {
    dispatch_semaphore_wait(semaphore, 0);
    
    [self setLabels: countryInformations];
    [self setMap: countryInformations];

    UIImage * flag = [self prepareFlag:countryInformations.flag];
    [self setFlag:flag];
}

- (void)setLabels:(struct RestCountryFullInfo) countryInformations {
    void (^setLabels)(void) = ^void() {
        self.mainNameLabel.text = countryInformations.name;
        self.secondaryNameLabel.text = countryInformations.altSpellings.lastObject;
        self.capitalNameLabel.text = countryInformations.capital;
        self.populationCountLabel.text = [NSString stringWithFormat: @"%i", countryInformations.population];
        self.areaSizeLabel.text = [NSString stringWithFormat:@"%0.0f km²", countryInformations.area];
    };
    
    dispatch_async(dispatch_get_main_queue(), setLabels);
}

- (void)setMap:(struct RestCountryFullInfo) countryInformations {
    float distance = [self aproximateDistance: countryInformations.area];
    CLLocationDistance clLocationDistance = (CLLocationDistance)distance;
    
    float latitude = [countryInformations.latlng[0] floatValue];
    float longitude = [countryInformations.latlng[1] floatValue];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);

    MKMapCamera * camera = [MKMapCamera cameraLookingAtCenterCoordinate:coordinate fromDistance:clLocationDistance pitch:0 heading:0];
    
    void (^setMap)(void) = ^void() {
        [self.mapView setCenterCoordinate:coordinate animated:false];
        [self.mapView setCamera:camera animated: true];
    };
    
    dispatch_async(dispatch_get_main_queue(), setMap);
}

- (float)aproximateDistance:(float) area {
    return 3.5 * sqrt(1.8 * area) * 1000;
}

- (UIImage *)prepareFlag:(NSURL *)flagURL {
    SVGKImage * svgFlag = nil;
    
    @try {
        NSData * data = [NSData dataWithContentsOfURL:flagURL];
        svgFlag = [SVGKImage imageWithData:data];
    }
    @catch (NSException *exception) {
        NSLog(@"image processor error");
        NSLog(@"%@", exception.reason);
    }
    
    return svgFlag.UIImage;
}

- (void)setFlag:(UIImage *)flagImage {
    dispatch_async(dispatch_get_main_queue(), ^void(){ self.flagImage.image = flagImage;});
}
@end
