//
//  TableCell.m
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import "TableCell.h"

@implementation TableCell

-(void) setCountryName:(NSString *) name {
    _countryNameLabel.text = name;
}
@end
