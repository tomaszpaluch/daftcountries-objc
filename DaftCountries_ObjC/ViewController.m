//
//  ViewController.m
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import "ViewController.h"
#import "CountryDetailsViewController.h"
#import "RestService.h"
#import "TableCell.h"
#import "RestCountryFullInfo.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

RestService * restService = nil;
NSTimer *searchTimer = nil;

-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    __weak ViewController *weakSelf = self;
    void (^reloadAction)(void) = ^void() {
        [weakSelf.tableView reloadData];
    };
    
    restService = [[RestService alloc] initWithReloadAction: reloadAction];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _searchBar.delegate = self;
    
    [restService getAllCountries];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        __weak CountryDetailsViewController * destVC = (CountryDetailsViewController *)[segue destinationViewController];
        
        int selectedRow = (int)[[self.tableView indexPathForSelectedRow] row];
        
        void (^completion)(struct RestCountryFullInfo) = ^(struct RestCountryFullInfo info) {
            [destVC setupView:info];
        };
        
        [restService getDetailedInformationsOfACountryAt:selectedRow withCompletion:completion];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    TableCell * cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
        
    NSString * countryName = [restService getNameAt: indexPath.row];
    [cell setCountryName:countryName];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [restService getLastInquiryCount];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchTimer != nil)
        [searchTimer invalidate];
    
    void (^completion)(NSTimer *) = ^void(NSTimer * timer) {
        if ([searchText length] > 0) {
            [restService getCountriesBy: searchText];
        } else {
            [restService getAllCountries];
        }
    };
    
    searchTimer = [NSTimer scheduledTimerWithTimeInterval: 1 repeats: false block:completion];
}

@end
    

