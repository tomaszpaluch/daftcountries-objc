//
//  TableCell.h
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *countryNameLabel;

-(void) setCountryName:(NSString *) name;

@end

NS_ASSUME_NONNULL_END
