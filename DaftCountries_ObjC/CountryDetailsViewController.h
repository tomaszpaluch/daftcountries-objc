//
//  CountryDetailsViewController.h
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestCountryFullInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface CountryDetailsViewController : UIViewController
- (void)setupView:(struct RestCountryFullInfo) countryInformations;

@end

NS_ASSUME_NONNULL_END
