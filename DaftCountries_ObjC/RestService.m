//
//  RestService.m
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import "RestService.h"

@implementation RestService

NSMutableArray * lastInquiry = nil;
NSURL * url = nil;
void (^reloadAction)(void);

- (id) initWithReloadAction:(void (^_Nonnull)(void))reloadActionHandler {
    self = [super init];
    
    lastInquiry = [[NSMutableArray alloc] init];
    url = [[NSURL alloc] initWithString:@"https://restcountries.eu/rest/v2/"];
    reloadAction = reloadActionHandler;
    
    return self;
}

- (void) getAllCountries {
    NSString * pathComponent = @"all";
    [self processGetingCountriesBy: pathComponent];
}

- (void) getCountriesBy:(NSString *) phrase {
    NSString * pathComponent = [NSString stringWithFormat:@"name/%@/", phrase];
    [self processGetingCountriesBy: pathComponent];
}

- (void) processGetingCountriesBy:(NSString *) pathComponent {
    NSURLQueryItem *tempQueryItem = [NSURLQueryItem queryItemWithName: @"fields" value: @"name"];
    NSArray * tempQueryItemsArray = [NSArray arrayWithObjects: tempQueryItem, nil];
    NSURL * actualUrl = [self createActualURL: pathComponent with: tempQueryItemsArray];
    
    [lastInquiry removeAllObjects];
    
    void (^completion)(NSArray *) = ^void(NSArray * response) {
        [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop){
            [lastInquiry addObject: obj[@"name"]];
        }];
        
        dispatch_async(dispatch_get_main_queue(), reloadAction);
    };
    
    [self makeInquiry:actualUrl and:completion];
}

- (void) getDetailedInformationsOfACountryAt:(int)index withCompletion:(void(^)(struct RestCountryFullInfo))completion  {
    NSString * name = [self getNameAt: index];
    NSString *pathComponent = [NSString stringWithFormat:@"name/%@/", name];
    
    NSURLQueryItem *tempQueryItem = [NSURLQueryItem queryItemWithName: @"fields" value: @"name;capital;altSpellings;population;latlng;area;flag"];
    NSArray * tempQueryItemsArray = [NSArray arrayWithObjects: tempQueryItem, nil];
    NSURL * actualUrl = [self createActualURL: pathComponent with: tempQueryItemsArray];
    
    void (^bigCompletion)(NSArray *) = ^void(NSArray * response) {
        struct RestCountryFullInfo info;
        
        info.name = response[0][@"name"];
        info.capital = response[0][@"capital"];
        info.altSpellings = response[0][@"altSpellings"];
        info.population = [response[0][@"population"] intValue];
        info.latlng = response[0][@"latlng"];
        info.area = [response[0][@"area"] floatValue];
        info.flag = [NSURL URLWithString:response[0][@"flag"]];

        completion(info);
    };
    
    [self makeInquiry:actualUrl and:bigCompletion];
}

- (NSURL *) createActualURL:(NSString *)pathComponent with: (NSArray<NSURLQueryItem *> *) queryItems {
    
    NSURL * tempUrl = [url URLByAppendingPathComponent: (pathComponent)];
    
    NSURLComponents * urlComponents = [[NSURLComponents alloc] initWithString: [tempUrl absoluteString]];
    
    [urlComponents setQueryItems: queryItems];
    
    return [urlComponents URL];
}

- (void) makeInquiry:(NSURL *)url and:(void (^)(NSArray *))completion {
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod: @"GET"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
     
                if (!error) {
                    //if ([response isKindOfClass:[NSHTTPURLResponse class]] || [response isKindOfClass:[NSURLResponse class]] ) {
                        NSError *jsonError;
                        NSArray *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
    
                        if (jsonError) {
                            NSLog(@"error : %@", jsonError.description);
                        } else {
                            completion(jsonResponse);
                        }
                   // }  else {
                        //Web server is returning an error
                    //}
                } else {
                    NSLog(@"error : %@", error.description);
                }
            }] resume];
    /*
    URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
        do {
            let jsonDecoder = JSONDecoder()
            let responseModel = try jsonDecoder.decode([T].self, from: data!)
            completion(responseModel)
        } catch {
            print("JSON Serialization error \n\(error)")
        }
    }).resume()*/
}

-( NSString *) getNameAt:(NSUInteger) index {
    return lastInquiry[index];
}

- (NSUInteger) getLastInquiryCount {
    return lastInquiry.count;
}

- (void)TEST_getAllCountries:(NSURL *)url {
    __block BOOL readyToGo = false;
    
    void (^completion)(NSArray *) = ^void(NSArray * response) {
        [response enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL * stop){
            [lastInquiry addObject: obj[@"name"]];
        }];
    };
    
    [self makeInquiry:url and:completion];
}

- (struct RestCountryFullInfo)TEST_getCountryBy:(NSURL *)url {
    __block struct RestCountryFullInfo restCountryFullInfo;
    __block BOOL readyToGo = false;
    
    void (^completion)(NSArray *) = ^void(NSArray * response) {
        restCountryFullInfo.name = response[0][@"name"];
        restCountryFullInfo.capital = response[0][@"capital"];
        restCountryFullInfo.altSpellings = response[0][@"altSpellings"];
        restCountryFullInfo.population = [response[0][@"population"] intValue];
        restCountryFullInfo.latlng = response[0][@"latlng"];
        restCountryFullInfo.area = [response[0][@"area"] floatValue];
        restCountryFullInfo.flag = [NSURL URLWithString:response[0][@"flag"]];
        
        readyToGo = true;
    };
    
    [self makeInquiry:url and:completion];
    
    while(!readyToGo) {}
    return restCountryFullInfo;
}
@end
