//
//  RestService.h
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestCountryFullInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface RestService : NSObject
- (id) initWithReloadAction:(void (^_Nonnull)(void))reloadActionHandler;
- (void) getAllCountries;
- (void) getCountriesBy:(NSString *) phrase;
- (void) getDetailedInformationsOfACountryAt:(int)index withCompletion:(void(^)(struct RestCountryFullInfo))completion;
- (NSString *) getNameAt:(NSUInteger) index;
- (NSUInteger) getLastInquiryCount;

- (void)TEST_getAllCountries:(NSURL *)url;
- (struct RestCountryFullInfo)TEST_getCountryBy:(NSURL *)url;
@end

NS_ASSUME_NONNULL_END
