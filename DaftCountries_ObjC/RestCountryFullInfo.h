//
//  RestCountryFullInfo.h
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#ifndef RestCountryFullInfo_h
#define RestCountryFullInfo_h

struct RestCountryFullInfo {
    NSString * name;
    NSString * capital;
    NSArray<NSString *>* altSpellings;
    int population;
    NSArray<NSNumber *>* latlng;
    float area;
    NSURL * flag;
};


#endif /* RestCountryFullInfo_h */
