//
//  ViewController.h
//  DaftCountries_ObjC
//
//  Created by tomaszpaluch on 18/06/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end

